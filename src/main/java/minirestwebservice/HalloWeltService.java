package minirestwebservice;

import org.apache.commons.lang3.StringUtils;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path(HalloWeltService.webContextPath)
public class HalloWeltService {
  static final String webContextPath = "/pantograph";
  static PantographStore dataStore = new PantographStore();
  static PantographData lastUpdate = null;

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public PantographData getLabelForId(@QueryParam("pictureId") String pictureId) {
    checkDataStore();
    if (StringUtils.isBlank(pictureId)) {
      throw new WebApplicationException("Missing pictureId", Response.Status.BAD_REQUEST);
    }

    if (dataStore.getDataStore() == null) {
      throw new WebApplicationException("No data available", Response.Status.NOT_FOUND);
    }

    PantographData data = dataStore.getDataStore().get(pictureId);

    if (data == null) {
      throw new WebApplicationException(
          "No data available for pictureId=" + pictureId, Response.Status.NOT_FOUND);
    }

    return dataStore.getDataStore().get(pictureId);
  }

  @Path("/lastUpdate")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Response getLastUpdate() {
    if (lastUpdate == null) {
      new WebApplicationException("No Data", Response.Status.NOT_FOUND);
    }

    return Response.ok() //200
            .entity(lastUpdate)
            .header("Access-Control-Allow-Origin", "*")
            .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
            .allow("OPTIONS").build();
  }

  @GET
  @Path("/fake")
  @Produces(MediaType.APPLICATION_JSON)
  public PantographStore getLabelForIdJson() {
    checkDataStore();
    if (dataStore.getDataStore().isEmpty()) {
      fakeIt();
    }
    return dataStore;
  }

  @Path("/dataUnit")
  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  public void putNewLabelMap(@NotNull @Valid PantographData data) {
    checkDataStore();
    if (data == null) {
      throw new WebApplicationException("Missing data object", Response.Status.NO_CONTENT);
    }

    if (StringUtils.isBlank(data.id)) {
      throw new WebApplicationException("Missing id", Response.Status.NO_CONTENT);
    }

    if (dataStore.getDataStore().get(data.id) == null) {
        dataStore.getDataStore().put(data.id, data);
    } else if (data.first.thickness > -1) {
        dataStore.getDataStore().get(data.id).first.thickness = data.first.thickness;
        dataStore.getDataStore().get(data.id).second.thickness = data.second.thickness;
    } else {
        PantographData existingData = dataStore.getDataStore().get(data.id);
        existingData.first.abplatzung = data.first.abplatzung;
        existingData.first.created = data.first.created;
        existingData.first.diagonalCut = data.first.diagonalCut;
        existingData.first.predictedMaintenanceDate = data.first.predictedMaintenanceDate;
        existingData.first.location = data.first.location;
        existingData.first.trafficLight = data.first.trafficLight;

        existingData.second.abplatzung = data.second.abplatzung;
        existingData.second.created = data.second.created;
        existingData.second.diagonalCut = data.second.diagonalCut;
        existingData.second.predictedMaintenanceDate = data.second.predictedMaintenanceDate;
        existingData.second.location = data.second.location;
        existingData.second.trafficLight = data.second.trafficLight;
    }

    lastUpdate = dataStore.getDataStore().get(data.id);
  }

  @DELETE
  public void deleteDataStore() {
    dataStore.getDataStore().clear();
  }

  private void checkDataStore() {
    if (dataStore == null) {
      dataStore = new PantographStore();
    }
  }

  private void fakeIt() {

    PantographTags pantographTagsLeft =
        new PantographTags(
            "left",
            70,
            true,
            false,
            "2018-06-22T20:48:39.9671651Z",
            "2018-06-22T20:48:39.9671651Z",
            "green");
    PantographTags pantographTagsRight =
        new PantographTags(
            "right",
            45,
            false,
            false,
            "2018-06-22T20:48:39.9671651Z",
            "2018-06-22T20:48:39.9671651Z",
            "yellow");
    PantographData pantographData =
        new PantographData("1", pantographTagsLeft, pantographTagsRight);
    dataStore.getDataStore().put(pantographData.id, pantographData);

    PantographTags pantographTagsLeft2 =
        new PantographTags(
            "left",
            30,
            true,
            false,
            "2018-06-22T20:48:39.9671651Z",
            "2018-06-22T20:48:39.9671651Z",
            "red");
    PantographTags pantographTagsRight2 =
        new PantographTags(
            "right",
            45,
            false,
            true,
            "2018-06-22T20:48:39.9671651Z",
            "2018-06-22T20:48:39.9671651Z",
            "yellow");
    PantographData pantographData2 =
        new PantographData("2", pantographTagsLeft2, pantographTagsRight2);
    dataStore.getDataStore().put(pantographData2.id, pantographData2);

    PantographTags pantographTagsLeft3 =
        new PantographTags(
            "left",
            60,
            false,
            false,
            "2018-06-22T20:48:39.9671651Z",
            "2018-06-22T20:48:39.9671651Z",
            "green");
    PantographTags pantographTagsRight3 =
        new PantographTags(
            "right",
            50,
            false,
            false,
            "2018-06-22T20:48:39.9671651Z",
            "2018-06-22T20:48:39.9671651Z",
            "green");
    PantographData pantographData3 =
        new PantographData("3", pantographTagsLeft3, pantographTagsRight3);
    dataStore.getDataStore().put(pantographData3.id, pantographData3);

    lastUpdate = pantographData3;
  }
}

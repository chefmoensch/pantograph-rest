package minirestwebservice;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashMap;
import java.util.Map;

@XmlRootElement
public class PantographStore {

    public Map<String, PantographData> dataStore = new HashMap<>();

    public PantographStore() {
    }

    public Map<String, PantographData> getDataStore() {
        return dataStore;
    }

    public void setDataStore(Map<String, PantographData> dataStore) {
        this.dataStore = dataStore;
    }
}

package minirestwebservice;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PantographTags {
    String location;
    int thickness;
    boolean abplatzung;
    boolean diagonalCut;
    String created;
    String predictedMaintenanceDate;
    String trafficLight;

    public PantographTags() {

    }

    public PantographTags(String location, int thickness, boolean abplatzung, boolean diagonalCut, String created, String predictedMaintenanceDate, String trafficLight) {
        this.location = location;
        this.thickness = thickness;
        this.abplatzung = abplatzung;
        this.diagonalCut = diagonalCut;
        this.created = created;
        this.predictedMaintenanceDate = predictedMaintenanceDate;
        this.trafficLight = trafficLight;
    }

    public boolean isAbplatzung() {
        return abplatzung;
    }

    public boolean isDiagonalCut() {
        return diagonalCut;
    }

    public void setDiagonalCut(boolean diagonalCut) {
        this.diagonalCut = diagonalCut;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getThickness() {
        return thickness;
    }

    public void setThickness(int thickness) {
        this.thickness = thickness;
    }

    public boolean getAbplatzung() {
        return abplatzung;
    }

    public void setAbplatzung(boolean abplatzung) {
        this.abplatzung = abplatzung;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getPredictedMaintenanceDate() {
        return predictedMaintenanceDate;
    }

    public void setPredictedMaintenanceDate(String predictedMaintenanceDate) {
        this.predictedMaintenanceDate = predictedMaintenanceDate;
    }

    public String getTrafficLight() {
        return trafficLight;
    }

    public void setTrafficLight(String trafficLight) {
        this.trafficLight = trafficLight;
    }
}

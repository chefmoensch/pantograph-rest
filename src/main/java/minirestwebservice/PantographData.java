package minirestwebservice;

import javax.xml.bind.annotation.XmlRootElement;

public class PantographData {

    String id;
    PantographTags first;
    PantographTags second;

    public PantographData() {

    }

    public PantographData(String id, PantographTags left, PantographTags right) {
        this.id = id;
        this.first = left;
        this.second = right;
    }

    public PantographTags getFirst() {
        return first;
    }

    public void setFirst(PantographTags first) {
        this.first = first;
    }

    public PantographTags getSecond() {
        return second;
    }

    public void setSecond(PantographTags second) {
        this.second = second;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}